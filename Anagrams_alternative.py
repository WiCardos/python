userText1 = input("What is your first text? ")
userText2 = input("What is your second text? ")

class NotAnagrams(Exception):   #creates a functional exception
    pass

try:
    if len(userText1.replace(" ",'')) <= 0 or len(userText2.replace(" ",'')) <= 0:
        raise ValueError

    userText1 = userText1.upper().replace(" ",'')
    userText2 = userText2.upper().replace(" ",'')

    userText1 = sorted(userText1)
    userText2 = sorted(userText2)

    for i in range(len(userText1)):
        if userText1[i] != userText2[i]:
            raise NotAnagrams #print("Not anagrams")
    else:
        print("Anagrams") #this will only be printed if the for loops are not interruped.

except ValueError:
    print("You must write something!")
except NotAnagrams:
    print("Not anagrams")
except:
    print("Something didn't go as expected!")    

#Test Data
#input1: Listen
#input2: Silent
#expected output:   Anagrams
#input1: modern
#input2: norman
#expected output:   Not anagrams
