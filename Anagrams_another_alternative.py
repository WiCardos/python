userText1 = input("What is your first text? ")
userText2 = input("What is your second text? ")

try:
    if len(userText1.replace(" ",'')) <= 0 or len(userText2.replace(" ",'')) <= 0:
        raise ValueError

    userText1 = userText1.upper().replace(" ",'')
    userText2 = userText2.upper().replace(" ",'')

    userText1 = ''.join(sorted(userText1))
    userText2 = ''.join(sorted(userText2))

    if userText1 == userText2:
        print("Anagrams")
    else:
        print("Not anagrams")

except ValueError:
    print("You must write something!")
except:
    print("Something didn't go as expected!")    

#Test Data
#input1: Listen
#input2: Silent
#expected output:   Anagrams
#input1: modern
#input2: norman
#expected output:   Not anagrams
