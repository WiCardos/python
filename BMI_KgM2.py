try:

    l_weight = float(input("What is your weight, if you don't miond me asking?(In kilograms, use \".\" as a decimal separator) "))
    l_height = float(input("What is your height? (In meters, use \".\" as a decimal separator) "))

    def f_bmi(weight, height):
        return weight / height ** 2

    l_bmi = round(f_bmi(l_weight,l_height), 2)
    print("This is your Body Mass Index: ", l_bmi, "kg/m2")

    if (l_bmi < 16):                        l_message = "Severe Thinness"
    elif (l_bmi >= 16   and l_bmi < 17):    l_message = "Moderate Thinness"
    elif (l_bmi >= 17   and l_bmi < 18.5):  l_message = "Mild Thinness"
    elif (l_bmi >= 18.5 and l_bmi < 25):    l_message = "Normal"
    elif (l_bmi >= 25   and l_bmi < 30):    l_message = "Overweight"
    elif (l_bmi >= 30   and l_bmi < 35):    l_message = "Obese Class I"
    elif (l_bmi >= 35   and l_bmi < 40):    l_message = "Obese Class II"
    elif l_bmi >= 40:                       l_message = "Obese Class III"

    print("you are on the category: ", l_message, "!", sep="")

except:
    print("Something didn't go as expected!")    
