try:
    l_list = []
        
    swapped = True

    l_next_value = input("What is your first element of the list? ")

    while l_next_value.lower() != "stop":
        l_list.append(l_next_value)
        l_next_value = input("What is your next element of the list? \nWrite \"Stop\" is you wnat to stop inserting elements to the list. ")

    print("This is your list: ", l_list)

    while swapped:
        swapped = False
        for element in range(len(l_list) - 1):
            if l_list[element] > l_list[element + 1]:
                swapped = True
                l_list[element], l_list[element + 1] = l_list[element + 1], l_list[element]

    print("And this is your sorted list: ", l_list)
except:
    print("Something didn't go as expected!")
