userMessage = input("What is your message? ")
userShiftValue = int(input("What is your shift value? (In a range of 1..25, inclusive) "))

try:
    if userShiftValue not in range(1,26):
        raise ValueError
except ValueError:
    print("You must choose a number between 1 and 25!")


encryptedText = ''

for char in userMessage:

    if not char.isalpha():
        encryptedText += char
        continue
 
    #If the current character is a capital letter
    #and the new character is bigger than 'Z'
    #it starts from the begging of the capital letters
    if ord(char) in range(65,91) and ord(char) + userShiftValue > 90:
        code = 64 + ((ord(char)+userShiftValue)%90)

    #if the current character is a lower case letter
    #and the new character is bigger than 'z'
    #it starts from the begging of the lower case letters
    elif ord(char) in range(97,123) and ord(char) + userShiftValue > 122:
        code = 96 + ((ord(char)+userShiftValue)%122)

    #otherwise, it just "moves" the character's decimal userShiftValue times
    else:
        code = ord(char) + userShiftValue

    encryptedText += chr(code)

print(encryptedText)

#Teste Data:
#
#
#abcxyzABCxyz 123
#2
#expected: cdezabCDEzab 123
#
#The die is cast
#25
#expected: Sgd chd hr bzrs
