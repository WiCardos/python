from os import strerror

#creating the dicitionay with the letter counts
l_dictionaryCounts = {}

for i in range(97,123):
    l_dictionaryCounts[chr(i)] = 0

l_srcname = input("Enter the source file name: ")

try:
    for i in open(l_srcname, 'rt').read():
        l_dictionaryCounts[i.lower()]+=1
except IOError as e:
    print("Cannot open the source file: ", strerror(e.errno))
    exit(e.errno)	

for i in l_dictionaryCounts:
    if l_dictionaryCounts[i]!=0:
        print(i, '->', l_dictionaryCounts[i])

#Test Data
#file named 'samplefile.txt' (must be in the same dir as this script)
#with 'aBc'
#expected outputs:
#a -> 1
#b -> 1
#c -> 1
