def liters_100km_to_miles_gallon(liters):
    
    #First convert both units
    #liters to gallons
    #km to miles
    gallons = liters/3.785411784
    miles = 100/1.609344
    #then just revers the gallons/mile to
    #mile per gallons
    mpg = miles/gallons
    return mpg

def miles_gallon_to_liters_100km(miles):
    
    #First convert both units
    #gallons to liters
    #miles to km
    liters = 3.785411784 #(gallons)
    kms = miles*1.609344 #(1km is 1000meters)
    #then find the ration of liters per km
    lpk = liters/kms
    #must multiply the ration by 100, because it is 1l per 100km
    return lpk*100
    
print(liters_100km_to_miles_gallon(3.9))
print(liters_100km_to_miles_gallon(7.5))
print(liters_100km_to_miles_gallon(10.))
print(miles_gallon_to_liters_100km(60.3))
print(miles_gallon_to_liters_100km(31.4))
print(miles_gallon_to_liters_100km(23.5))

#Test Data
#3.9
#7.5
#10.
#60.3
#31.4
#23.5
#expected outputs:
#60.31143162393162
#31.36194444444444
#23.52145833333333
#3.9007393587617467
#7.490910297239916
#10.009131205673757
