def is_year_leap(year):
    if year%4==0 and (year%100!=0 or year%400==0):
        l_val = True
    else:
        l_val = False
    return l_val

def day_of_year(year, month, day):
    #We sart with a local list of days, per month.
    #This is rebuild every time the function is summoned.
    l_days_months = [31,28,31,30,31,30,31,31,30,31,30,31]
    
    #if it is a leap year all we have to do is add 1 day to February!
    if is_year_leap(year):
        l_days_months[1] += 1
    
    #Then we sum all days of previous months.    
    total_days = 0
    for i in range(len(l_days_months[0:month-1])):
        total_days += l_days_months[i]

    #And finally, we add the last days of the current month
    total_days += day
    
    return total_days


print(day_of_year(2000, 12, 31))
print(day_of_year(2000, 1, 10))
print(day_of_year(2000, 3, 1))
