class WeekDayError(Exception):
    pass

class Weeker:
    __weekList = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun']

    def __init__(self,dayOfWeek):
        if dayOfWeek not in Weeker.__weekList:
            raise WeekDayError
        self.__v_dayOfWeek = dayOfWeek

    def __str__(self):
        return self.__v_dayOfWeek

    def add_days(self, n):
        l_var = Weeker.__weekList.index(self.__v_dayOfWeek)
        l_var += n%7
        self.__v_dayOfWeek = Weeker.__weekList[l_var]
    
    def subtract_days(self, n):
        l_var = Weeker.__weekList.index(self.__v_dayOfWeek)
        l_var -= n%7
        self.__v_dayOfWeek = Weeker.__weekList[l_var]


try:
    weekday = Weeker('Mon')
    print(weekday)
    weekday.add_days(15)
    print(weekday)
    weekday.subtract_days(23)
    print(weekday)
    weekday = Weeker('Monday')
except WeekDayError:
    print("Sorry, I can't serve your request.")

#Test Data
#expected outputs:
#Mon
#Tue
#Sun
#Sorry, I can't serve your request.
