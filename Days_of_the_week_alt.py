class WeekDayError(Exception):
    pass

class Weeker:
    __weekList = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun']

    def __init__(self,dayOfWeek):
        try:
            self.__current = Weeker.__weekList.index(dayOfWeek)
        except ValueError:
            raise WeekDayError

    def __str__(self):
        return Weeker.__weekList[self.__current]

    def add_days(self, n):
        self.__current = (self.__current + n) % 7
    
    def subtract_days(self, n):
        self.__current = (self.__current - n) % 7


try:
    weekday = Weeker('Mon')
    print(weekday)
    weekday.add_days(15)
    print(weekday)
    weekday.subtract_days(23)
    print(weekday)
    weekday = Weeker('Monday')
except WeekDayError:
    print("Sorry, I can't serve your request.")

#Test Data
#expected outputs:
#Mon
#Tue
#Sun
#Sorry, I can't serve your request.
