from os import strerror

l_dictionaryCounts = {}
l_srcname = input("Enter the source file name: ")

try:
    for i in open(l_srcname, 'rt').readlines():
        if i[0:18] in l_dictionaryCounts.keys():
            l_dictionaryCounts[i[0:18]] += float(i[18:])
        else:
            l_dictionaryCounts[i[0:18]] = float(i[18:])
except IOError as e:
    print("Cannot open the source file: ", strerror(e.errno))
    exit(e.errno)

l_sortedDictionaryCounts=dict(sorted(l_dictionaryCounts.items(), key =lambda item: item[0], reverse= False))

for i in l_sortedDictionaryCounts.keys():
    print(i, l_sortedDictionaryCounts[i])

#Test Data
#file named 'samplefile.txt' (must be in the same dir as this script)
#with:
#John     Smith    5
#Anna     Boleyn   4.5
#John     Smith    2
#Anna     Boleyn   11
#Andrew   Cox      1.5

#expected outputs:
#Andrew   Cox      1.5
#Anna     Boleyn   15.5
#John     Smith    7.0
