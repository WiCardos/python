def f_even_or_odd(l_number):
    if l_number % 2 == 0:
        l_output = "Even"
    else:
        l_output = "Odd"
    return l_output


l_var = True
while l_var:
    try:
        u_number = int(input("What is your number? "))

        print("Your number is a ", f_even_or_odd(u_number))

        l_var = False
    except  ValueError:
        print("You must write an integer")
