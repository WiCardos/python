#https://leetcode.com/problems/add-binary/

class Solution:
    def addBinary(self, a: str, b: str) -> str:
        return bin(int(a,2)+int(b,2))[2:] 

#Test Data

#input1:
#a = "11", b = "1"
#expected output:
#"100"

#input2:
#a = "1010", b = "1011"
#expected output:
#"10101"
