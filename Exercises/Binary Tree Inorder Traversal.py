#https://leetcode.com/problems/binary-tree-inorder-traversal/description/
"""
Given the root of a binary tree, return the inorder traversal of its nodes' values.
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def inorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        if root:
            l_output = self.inorderTraversal(root.left) + [root.val] + self.inorderTraversal(root.right)
        else:
            l_output = []
        return l_output


#Test Data

#input1:
#root = [1,null,2,3]
#expected output:
#[1,3,2]

#input2:
#root = []
#expected output:
#[]

#input3:
#root = [1]
#expected output:
#[1]
