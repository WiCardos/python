#https://leetcode.com/problems/climbing-stairs/description/
#https://brilliant.org/wiki/permutations-with-repetition/
#You are climbing a staircase. It takes n steps to reach the top.
#Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

import math
class Solution:
    def climbStairs(self, n: int) -> int:
        l_output = 1 #for 1 steps only
        
        for i in range(n//2): #how many times we can use 2 steps
            
            l_list = []
            l_count = 0

            while l_count < (i+1):
                l_list.append(2)
                l_count += 1

            while sum(l_list) < n:
                l_list.append(1)

            #length! / (#1! * "2!)
            l_output += (math.factorial(len(l_list)) / (math.factorial(l_list.count(2))*math.factorial(l_list.count(1))))

        return int(l_output)

#Test Data

#input1:
#n = 2
#expected output:
#2

#input2:
#n = 3
#expected output:
#3
