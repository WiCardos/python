#https://leetcode.com/problems/find-the-index-of-the-first-occurrence-in-a-string/

class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        l_output = -1
        if needle in haystack:
            l_output = haystack.index(needle)
        return l_output

#Test Data

#input1:
#haystack = "sadbutsad", needle = "sad"
#expected output:
#0

#input2:
#haystack = "leetcode", needle = "leeto"
#expected output:
#-1
