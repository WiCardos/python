#https://leetcode.com/problems/length-of-last-word/

class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        l_list = s.split(' ')
        l_val = True
        while l_val:
            if l_list.count('')>0:
                l_list.remove('')
            else:
                l_val = False
        return len(l_list[-1])


#Test Data

#input1:
#s = "Hello World"
#expected output:
#5

#input2:
#s = "   fly me   to   the moon  "
#expected output:
#4

#input3:
#s = "luffy is still joyboy"
#expected output:
#6
