#https://leetcode.com/problems/longest-common-prefix/

class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:

        l_output = ""
        l_true = False

        for letter in range(len(strs[0])):
            
            for word in range(len(strs)):
                
                if letter < len(strs[word]) and strs[0][letter] == strs[word][letter]:
                    l_true = True
                else:
                    l_true = False
                    break
            
            if l_true:
                l_output += strs[0][letter]
            else:
                break

        return l_output

#Test Data

#input1:
#strs = ["flower","flow","flight"]
#expected output:
#"fl"

#input2:
#strs = ["dog","racecar","car"]
#expected output:
#""

#input3:
#strs = ["a"]
#expected output:
#"a"

#input4:
#strs = ["aaa","aa","aaa"]
#expected output:
#"aa"
