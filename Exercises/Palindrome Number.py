#https://leetcode.com/problems/palindrome-number/

class Solution:
    def isPalindrome(self, x: int) -> bool:

        x_str = str(x)
        l_val = True

        for i in range(len(x_str)):
            if x_str[i] != x_str[-(i+1)]:
                l_val = False
        return l_val

#Test Data

#input1:
#x = 121
#expected output:
#true

#input2:
#x = -121
#expected output:
#false

#input3:
#x = 10
#expected output:
#false
