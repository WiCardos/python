#https://leetcode.com/problems/plus-one/

class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:

        digits.reverse()
        l_newVal = digits[0] + 1
        l_previous = l_newVal // 10
        digits[0] = l_newVal % 10

        for i in range(len(digits)):
            if i != 0:
                l_newVal = digits[i] + l_previous
                l_previous = l_newVal // 10
                digits[i] = l_newVal % 10

        if l_previous > 0:
            digits.append(l_previous)

        digits.reverse()

        return digits

#Test Data

#input1:
#digits = [1,2,3]
#expected output:
#[1,2,4]

#input2:
#digits = [4,3,2,1]
#expected output:
#[4,3,2,2]

#input3:
#digits = [9]
#expected output:
#[1,0]
