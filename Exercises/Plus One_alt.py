#https://leetcode.com/problems/plus-one/

class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        l_str = ''
        l_list = []

        for i in digits:
            l_str = l_str + str(i)

        l_str = str(int(l_str) + 1)

        for i in l_str:
            l_list.append(int(i))

        return l_list

#Test Data

#input1:
#digits = [1,2,3]
#expected output:
#[1,2,4]

#input2:
#digits = [4,3,2,1]
#expected output:
#[4,3,2,2]

#input3:
#digits = [9]
#expected output:
#[1,0]
