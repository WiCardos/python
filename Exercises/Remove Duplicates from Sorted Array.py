#https://leetcode.com/problems/remove-duplicates-from-sorted-array/

class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        for i in nums:
            while nums.count(i) != 1:
                nums.remove(i)
        return len(nums)

#Test Data

#input1:
#nums = [1,1,2]
#expected output:
#2

#input2:
#nums = [0,0,1,1,1,2,2,3,3,4]
#expected output:
#5
