#https://leetcode.com/problems/remove-element/

class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        while val in nums:
            nums.remove(val)
        return len(nums)

#Test Data

#input1:
#nums = [3,2,2,3], val = 3
#expected output:
#2

#input2:
#nums = [0,1,2,2,3,0,4,2], val = 2
#expected output:
#5
