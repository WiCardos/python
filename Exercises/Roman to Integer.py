#https://leetcode.com/problems/roman-to-integer/

class Solution:
    def romanToInt(self, s: str) -> int:

        l_romanValue = {
            'I':1
            ,'V':5
            ,'X':10
            ,'L':50
            ,'C':100
            ,'D':500
            ,'M':1000
            }
        l_output = 0

        for i in range(len(s)):
            if i+1 < len(s) and l_romanValue[s[i]] < l_romanValue[s[i+1]]:
                l_output -= l_romanValue[s[i]]
            else:
                l_output += l_romanValue[s[i]]
        return l_output

#Test Data

#input1:
#nums = s = "III"
#expected output:
#3

#input2:
#s = "LVIII"
#expected output:
#58

#input3:
#s = "MCMXCIV"
#expected output:
#1994
