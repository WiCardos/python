#https://leetcode.com/problems/search-insert-position/

class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        if target in nums:
            l_output = nums.index(target)
        else:
            nums.append(target)
            nums.sort()
            l_output = nums.index(target)
        return l_output

#Test Data

#input1:
#nums = [1,3,5,6], target = 5
#expected output:
#2

#input2:
#nums = [1,3,5,6], target = 2
#expected output:
#1

#input3:
#nums = [1,3,5,6], target = 7
#expected output:
#4
