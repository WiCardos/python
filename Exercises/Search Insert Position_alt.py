#https://leetcode.com/problems/search-insert-position/
#O(log n)

class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        l_bound, u_bound = 0, len(nums)-1
        while l_bound <= u_bound:
            middle = (u_bound + l_bound) // 2
            if target <= nums[middle]:
                u_bound = middle - 1
            elif target >= nums[middle]:
                l_bound = middle + 1
            else:
                return middle
        return l_bound

#Test Data

#input1:
#nums = [1,3,5,6], target = 5
#expected output:
#2

#input2:
#nums = [1,3,5,6], target = 2
#expected output:
#1

#input3:
#nums = [1,3,5,6], target = 7
#expected output:
#4
