#https://leetcode.com/problems/sqrtx/

#Given a non-negative integer x, return the square root of x rounded down to the nearest integer. The returned integer should be non-negative as well.
#You must not use any built-in exponent function or operator.
#For example, do not use pow(x, 0.5) in c++ or x ** 0.5 in python.

#O(log n)

class Solution:
    def mySqrt(self, x: int) -> int:
        l_lowBound = 0
        l_upBound = x

        while l_lowBound <= l_upBound:
            l_mid = (l_upBound + l_lowBound)//2
            if l_mid ** 2 == x:
              return l_mid
            elif l_mid ** 2 > x:
              l_upBound = l_mid-1
            elif l_mid ** 2 < x:
              l_lowBound = l_mid+1
        return l_upBound

#Test Data

#input1:
#x = 4
#expected output:
#2

#input2:
#x = 8
#expected output:
#2
