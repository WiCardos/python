#https://leetcode.com/problems/sqrtx/

#Given a non-negative integer x, return the square root of x rounded down to the nearest integer. The returned integer should be non-negative as well.
#You must not use any built-in exponent function or operator.
#For example, do not use pow(x, 0.5) in c++ or x ** 0.5 in python.

class Solution:
    def mySqrt(self, x: int) -> int:
        number=0
        while number**2 <= x:
            number+=1
        return number-1

#Test Data

#input1:
#x = 4
#expected output:
#2

#input2:
#x = 8
#expected output:
#2
