#https://leetcode.com/problems/two-sum/

class Solution:
    def twoSum(self, nums: list[int], target: int) -> list[int]:
        l_target = [0,0]
        for i in range(len(nums)):
            for j in range(len(nums)):
                if i!= j and nums[i] + nums[j] == target:
                    l_target[0] = i
                    l_target[1] = j
                    return l_target

#Test Data

#input1:
#nums = [2,7,11,15], target = 9
#expected output:
#[0,1]

#input2:
#nums = [3,2,4], target = 6
#expected output:
#[1,2]

#input3:
#nums = [3,3], target = 6
#expected output:
#[0,1]
