#https://leetcode.com/problems/two-sum/

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        pairs = {}
        for i in range(len(nums)):
            if nums[i] in pairs: 
                return[i, pairs[nums[i]]]
            pairs[target - nums[i]] = i

#Test Data

#input1:
#nums = [2,7,11,15], target = 9
#expected output:
#[0,1]

#input2:
#nums = [3,2,4], target = 6
#expected output:
#[1,2]

#input3:
#nums = [3,3], target = 6
#expected output:
#[0,1]
