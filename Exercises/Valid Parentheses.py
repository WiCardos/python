#https://leetcode.com/problems/valid-parentheses/

class Solution:
    def isValid(self, s: str) -> bool:
        
        l_val_roundBracket = []
        l_val_curlyBracket = []
        l_val_squareBracket = []
        
        l_output_2 = True

        for i,c in enumerate(s):

            if c == '(':
                l_val_roundBracket.append(i)
            elif c =='{':
                l_val_curlyBracket.append(i)
            elif c =='[':
                l_val_squareBracket.append(i)
            
            elif c == ')':
                if len(l_val_roundBracket)>0:
                    if len(l_val_curlyBracket)>0:
                        if l_val_roundBracket[-1]>l_val_curlyBracket[-1]:
                            l_val_roundBracket.pop()
                        else:
                            l_output_2 = False
                            break
                    elif len(l_val_squareBracket)>0:
                        if l_val_roundBracket[-1]>l_val_squareBracket[-1]:
                            l_val_roundBracket.pop()
                        else:
                            l_output_2 = False
                            break
                    else:
                        l_val_roundBracket.pop()
                else:
                    l_output_2 = False
                    break
            elif c == '}':
                if len(l_val_curlyBracket)>0:
                    if len(l_val_roundBracket)>0:
                        if l_val_curlyBracket[-1]>l_val_roundBracket[-1]:
                            l_val_curlyBracket.pop()
                        else:
                            l_output_2 = False
                            break
                    elif len(l_val_squareBracket)>0:
                        if l_val_curlyBracket[-1]>l_val_squareBracket[-1]:
                            l_val_curlyBracket.pop()
                        else:
                            l_output_2 = False
                            break
                    else:
                        l_val_curlyBracket.pop()
                else:
                    l_output_2 = False
                    break
            elif c == ']':
                if len(l_val_squareBracket)>0:
                    if len(l_val_roundBracket)>0:
                        if l_val_squareBracket[-1]>l_val_roundBracket[-1]:
                            l_val_squareBracket.pop()
                        else:
                            l_output_2 = False
                            break
                    elif len(l_val_curlyBracket)>0:
                        if l_val_squareBracket[-1]>l_val_curlyBracket[-1]:
                            l_val_squareBracket.pop()
                        else:
                            l_output_2 = False
                            break
                    else:
                        l_val_squareBracket.pop()
                else:
                    l_output_2 = False
                    break
            else:
                l_output_2 = False
                break
    
        if l_output_2 == True and  len(l_val_roundBracket) == 0 and len(l_val_curlyBracket) == 0 and len(l_val_squareBracket) == 0:
            l_output = True
        else:
            l_output = False
        
        return l_output

#Test Data

#input1:
#s = "()"
#expected output:
#true

#input2:
#s = "()[]{}"
#expected output:
#true

#input3:
#s = "(]"
#expected output:
#false
