#https://leetcode.com/problems/valid-parentheses/

class Solution:
    def isValid(self, s: str) -> bool:
        l_output = True
        lst=[]
        dic={'(':')','{':'}','[':']'}
        left="([{"
        for i in s:
            if i in left:
                lst.append(i)
            elif lst and dic[lst[-1]]==i:
                lst.pop()
            else:
                l_output = False

        if lst:
            l_output = False

        return l_output

#Test Data

#input1:
#s = "()"
#expected output:
#true

#input2:
#s = "()[]{}"
#expected output:
#true

#input3:
#s = "(]"
#expected output:
#false
