class QueueError(IndexError):
    pass

class Queue:
    def __init__(self):
        self.__queue = []

    def put(self, val):
        self.__queue.append(val)

    def get(self):
        val = self.__queue[0]
        del self.__queue[0]
        return val
	

l_queue = Queue()
l_queue.put(1)
l_queue.put('dog')
l_queue.put(False)

try:
    for i in range(4):
        print(l_queue.get())
except:
    print("Queue error")

#Test Data
#expected outputs:
#1
#dog
#False
#Queue error
