class Queue:
    def __init__(self):
        self.queue = []

    def put(self, val):
        self.queue.append(val)

    def get(self):
        val = self.queue[0]
        del self.queue[0]
        return val
	
class SubQueue(Queue):
    def __init__(self):
        Queue.__init__(self)

    def c_empty(self):
        return len(self.queue) == 0

l_queue = SubQueue()
l_queue.put(1)
l_queue.put('dog')
l_queue.put(False)

for i in range(4):
    if not l_queue.c_empty():
        print(l_queue.get())
    else:
        print("Queue empty")

#Test Data
#expected outputs:
#1
#dog
#False
#Queue empty
