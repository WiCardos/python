try:
    g_number=int(input(
        "What is your number? "))

    if g_number < 0:
        print("You must choose a non-negative integer!")

    def f_factorial(l_number):
        if l_number < 2:
            return 1
        
        product = 1
        for i in range(2, l_number + 1):
            product *= i
        return product
    
    print(g_number, "! = ", f_factorial(g_number),sep="")

except:
    print("Something didn't go as expected!")
