#In this game, the player gets paid 1 monetary unit per pip on the dice's upper surface.
#When using multiple dices or multiple throws, the payout is on the best result.
#E.g.: On the first throw the player gets 2 pips, and on the second throw gets 5 pips, the payout is 5.

def f_fairValue(l_numDices):

    l_fairValue = 0

    for p in range(1, 7):
        l_fairValue += p * (p**l_numDices - (p - 1)**l_numDices)

    l_fairValue = 6**-l_numDices * l_fairValue

    return l_fairValue

l_val = True
while l_val:
    try:
        u_numberOfDices = int(input("How many dices/throws are you using? "))
        l_val = False
    except ValueError:
        print("You must choose an integer!")

print("The fair payment for this game is:", f_fairValue(u_numberOfDices))
