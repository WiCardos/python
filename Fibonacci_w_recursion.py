try:
    n_element=int(input(
        "Which element of the Fibonacci sequence do you wish to see? "))

    if n_element < 0:
        print("You must choose a non-negative integer!")

    def f_fibonacci(n_element):

        if n_element < 1:
            return None
        elif n_element < 3:
            return 1
        else:
            return f_fibonacci(n_element - 1) + f_fibonacci(n_element - 2)

    print(f_fibonacci(n_element))

except:
    print("Something didn't go as expected!")    
