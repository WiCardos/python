try:
    n_elements=int(input(
        "How many elements of the Fibonacci sequence do you wish to see? "))

    if n_elements < 0:
        print("You must choose a non-negative integer!")

    def f_fibonacci(n_elements):

        v_cur_elem, v_next_elem = 0, 1

        for i in range(n_elements):

            if v_cur_elem != 0:
                print(',', end='')

            print(v_cur_elem, end='')

            v_cur_elem, v_next_elem = v_next_elem, v_next_elem+v_cur_elem
        print('.')       


    f_fibonacci(n_elements)

except:
    print("Something didn't go as expected!")    
