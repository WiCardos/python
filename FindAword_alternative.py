userWord = input("What is your word? ")
userText = input("What is your text? ")

userWord = userWord.upper().replace(" ",'')
userText = userText.upper().replace(" ",'')

l_val = True

for letter in userWord:
    if userText.find(letter) == -1:
        print("No")
        l_val = False
        break

if l_val:
    print("Yes")

#Test Data
#input1: donor
#input2: Nabucodonosor
#expected output:   Yes
#input1: donut
#input2: Nabucodonosor
#expected output:   No
