from random import randint

l_cpu_numb = randint(0,10)

l_attempts = 0

l_val = True
while l_val:
    try:
        u_number = int(input("What is your guess? "))
        l_attempts += 1

        if u_number not in range(0,11):
            print("Your guess is not even in range!")
        elif u_number == l_cpu_numb:
            print("You guessed it in ", l_attempts, " tries!", sep='')
            l_val = False
        elif u_number < l_cpu_numb:
            print("Your guess is too low")
        else: #u_number > l_cpu_numb:
            print("Your guess is too high")

        if l_attempts == 5:
            print("You exceeded your attempts!")
            print("The number was: ", l_cpu_numb, "!", sep='')
            l_val = False

    except ValueError:
        print("You must choose an integer!")
