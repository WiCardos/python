def f_total_return(v_initial, v_reinvesment, v_interest, v_years):

    l_totalReinvestment = 0

    #the last year will not be considered because it is a reinvestment, which should only happen after year 1.
    for i in range(v_years-1):
        l_totalReinvestment += v_reinvesment * (1 + v_interest) ** (i+1) #i+1 because we don't want the power of 0!

    l_total = round(
        v_initial * (1 + v_interest) ** v_years + l_totalReinvestment 
        ,2)

    return l_total

l_var = True
while l_var:

    try:
        u_Initial       = float(input("What is your Initial investment?\n"))
        u_Reinvestment  = float(input("What is your yearly reinvestment?\n"))
        u_interestRate  = float(input("What is your expected interest rate? (1% is 0.01)\n"))
        l_var = False

    except ValueError:
        l_var = True
        print("You must write a float!")

l_var = True
while l_var:

    try:
        u_years         = int(input("For how many years is the investment taking place?\n"))
        l_var = False

    except ValueError:
        l_var = True
        print("You must write an integer for year!")

print(
    "Your expected return is: "
    ,f_total_return(
    v_initial       = u_Initial
    ,v_reinvesment  = u_Reinvestment
    ,v_interest     = u_interestRate
    ,v_years        = u_years
    )
)
