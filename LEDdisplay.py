try:

    l_listNumbers = input("What is your list of digits? (write all digits together e.g.: 12345) ")


    #initialize the variables
    line1 = ''
    line2 = ''
    line3 = ''
    line4 = ''
    line5 = ''

    for i in l_listNumbers:

        if len(line1) > 0:   #it is enought to validate only the first line, no need to validate line by line
            line1 = line1 + ' '
            line2 = line2 + ' '
            line3 = line3 + ' '
            line4 = line4 + ' '
            line5 = line5 + ' '

        if i == '0':
            line1 = line1 + "###"
            line2 = line2 + "# #"
            line3 = line3 + "# #"
            line4 = line4 + "# #"
            line5 = line5 + "###"
        elif i == '1':
            line1 = line1 + "  #"
            line2 = line2 + "  #"
            line3 = line3 + "  #"
            line4 = line4 + "  #"
            line5 = line5 + "  #"
        elif i == '2':
            line1 = line1 + "###"
            line2 = line2 + "  #"
            line3 = line3 + "###"
            line4 = line4 + "#  "
            line5 = line5 + "###"
        elif i == '3':
            line1 = line1 + "###"
            line2 = line2 + "  #"
            line3 = line3 + "###"
            line4 = line4 + "  #"
            line5 = line5 + "###"
        elif i == '4':
            line1 = line1 + "# #"
            line2 = line2 + "# #"
            line3 = line3 + "###"
            line4 = line4 + "  #"
            line5 = line5 + "  #"
        elif i == '5':
            line1 = line1 + "###"
            line2 = line2 + "#  "
            line3 = line3 + "###"
            line4 = line4 + "  #"
            line5 = line5 + "###"
        elif i == '6':
            line1 = line1 + "###"
            line2 = line2 + "#  "
            line3 = line3 + "###"
            line4 = line4 + "# #"
            line5 = line5 + "###"      
        elif i == '7':
            line1 = line1 + "###"
            line2 = line2 + "  #"
            line3 = line3 + "  #"
            line4 = line4 + "  #"
            line5 = line5 + "  #"
        elif i == '8':
            line1 = line1 + "###"
            line2 = line2 + "# #"
            line3 = line3 + "###"
            line4 = line4 + "# #"
            line5 = line5 + "###"
        elif i == '9':
            line1 = line1 + "###"
            line2 = line2 + "# #"
            line3 = line3 + "###"
            line4 = line4 + "  #"
            line5 = line5 + "###"

    print(line1)
    print(line2)
    print(line3)
    print(line4)
    print(line5)

except:
    print("Something didn't go as expected!")    
