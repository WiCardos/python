l_listNumbers = input("What is your list of digits? (write all digits together e.g.: 12345): ")

l_output = ['' for elem in range(5)]

l_count = False
for i in l_listNumbers:

    #if i is not a digits
    if i not in '0123456789':
        continue
    
    #to append a space only after the 1st digit.
    if l_count:
        for j in range(5):
            l_output[j] += ' '
    
    #empty matrix
    matrix = [ [' ',' ',' '] for k in range(5) ]
    
    if i == '0':
        matrix[0][0] = matrix[0][1] = matrix[0][2] = \
        matrix[1][0] = matrix[1][2] = \
        matrix[2][0] = matrix[2][2] = \
        matrix[3][0] = matrix[3][2] = \
        matrix[4][0] = matrix[4][1] = matrix[4][2] = '#'
    elif i == '1':
        matrix[0][2] = \
        matrix[1][2] = \
        matrix[2][2] = \
        matrix[3][2] = \
        matrix[4][2] = '#'
    elif i == '2':
        matrix[0][0] = matrix[0][1] = matrix[0][2] = \
        matrix[1][2] = \
        matrix[2][0] = matrix[2][1] = matrix[2][2] = \
        matrix[3][0] = \
        matrix[4][0] = matrix[4][1] = matrix[4][2] = '#'
    elif i == '3':
        matrix[0][0] = matrix[0][1] = matrix[0][2] = \
        matrix[1][2] = \
        matrix[2][0] = matrix[2][1] = matrix[2][2] = \
        matrix[3][2] = \
        matrix[4][0] = matrix[4][1] = matrix[4][2] = '#'
    elif i == '4':
        matrix[0][0] = matrix[0][2] = \
        matrix[1][0] = matrix[1][2] = \
        matrix[2][0] = matrix[2][1] = matrix[2][2] = \
        matrix[3][2] = \
        matrix[4][2] = '#'
    elif i == '5':
        matrix[0][0] = matrix[0][1] = matrix[0][2] = \
        matrix[1][0] = \
        matrix[2][0] = matrix[2][1] = matrix[2][2] = \
        matrix[3][2] = \
        matrix[4][0] = matrix[4][1] = matrix[4][2] = '#'
    elif i == '6':
        matrix[0][0] = matrix[0][1] = matrix[0][2] = \
        matrix[1][0] = \
        matrix[2][0]= matrix[2][1] = matrix[2][2] = \
        matrix[3][0] = matrix[3][2] = \
        matrix[4][0] = matrix[4][1] = matrix[4][2] = '#'
    elif i == '7':
        matrix[0][0] = matrix[0][1] = matrix[0][2] = \
        matrix[1][2] = \
        matrix[2][2] = \
        matrix[3][2] = \
        matrix[4][2] = '#'
    elif i == '8':
        matrix[0][0] = matrix[0][1] = matrix[0][2] = \
        matrix[1][0] = matrix[1][2] = \
        matrix[2][0] = matrix[2][1] = matrix[2][2] = \
        matrix[3][0] = matrix[3][2] = \
        matrix[4][0] = matrix[4][1] = matrix[4][2] = '#'
    elif i == '9':
        matrix[0][0] = matrix[0][1] = matrix[0][2] = \
        matrix[1][0] = matrix[1][2] = \
        matrix[2][0] = matrix[2][1] = matrix[2][2] = \
        matrix[3][2] = \
        matrix[4][0] = matrix[4][1] = matrix[4][2] = '#'

    #appending the output list
    for l in range(5):
        l_output[l] += ''.join(matrix[l])

    l_count = True
#printing the output list with all digits
for elem in l_output:
    print(elem)
