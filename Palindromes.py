userText = input("What is your text? ")

try:
    if len(userText) <= 0:
        raise ValueError
except ValueError:
    print("You must write something!")


userText = userText.upper().replace(" ",'')

for i in range(len(userText)):
    if userText[i] != userText[-(i+1)]:
        print("It's not a palindrome")
        break
else:
    print("It's a palindrome")


#Test Data
#input: Ten animals I slam in a net
#expected output:   It's a palindrome
#input: Eleven animals I slam in a net
#expected output:   It's not a palindrome
