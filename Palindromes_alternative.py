userText = input("What is your text? ")

try:
    if len(userText) <= 0:
        raise ValueError
except ValueError:
    print("You must write something!")


userText = userText.upper().replace(" ",'')

if userText == userText[::-1]:
    print("It's a palindrome")
else:
    print("It's not a palindrome")


#Test Data
#input: Ten animals I slam in a net
#expected output:   It's a palindrome
#input: Eleven animals I slam in a net
#expected output:   It's not a palindrome
