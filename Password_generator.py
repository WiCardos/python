#https://en.wikipedia.org/wiki/List_of_Unicode_characters

from random import randint

u_input = True
while u_input:
    try:
        u_length    = int(input("How many characters are in your password?\n"))
        u_input     = False
    except ValueError:
        print("You must choose an integer for length!")

u_digits        =   input("Do you want digits in your password?\n")
u_lower_case    =   input("Do you want lowercase characters in your password?\n")
u_upper_case    =   input("Do you want uppercase characters in your password?\n")
print("Do you want this set of symbols:", ''.join([chr(i) for i in range(32,47 +1)]), "in your password?")
u_set_1         =   input()
print("Do you want this set of symbols:", ''.join([chr(i) for i in range(58,64 +1)]), "in your password?")
u_set_2         =   input()
print("Do you want this set of symbols:", ''.join([chr(i) for i in range(91,96 +1)]), "in your password?")
u_set_3         =   input()
print("Do you want this set of symbols:", ''.join([chr(i) for i in range(123,126 +1)]), "in your password?")
u_set_4         =   input()

print("Do you have any special characters you wish to be included? (Just press 'Enter' otherwise.)")
print("Note that, since this is randomly generated, there is no guaranty they will be present in the final password!")
print("Put all characters concatenated without a separator, e.g.: €çÇ«»")
u_chr = input("")


#Building the user list of characters.
l_charList = []

if u_digits.upper() in ('YES','Y','1'):
    l_charList  +=  [*range(48, 57 +1)]
if u_lower_case.upper() in ('YES','Y','1'):
    l_charList  +=  [*range(97, 122 +1)]
if u_upper_case.upper() in ('YES','Y','1'):
    l_charList  +=  [*range(65, 90 +1)]
if u_set_1.upper() in ('YES','Y','1'):
    l_charList  +=  [*range(32,47 +1)]
if u_set_2.upper() in ('YES','Y','1'):
    l_charList  +=  [*range(58,64 +1)]
if u_set_3.upper() in ('YES','Y','1'):
    l_charList  +=  [*range(91,96 +1)]
if u_set_4.upper() in ('YES','Y','1'):
    l_charList  +=  [*range(123,126 +1)]
if len(u_chr) > 0:
    l_charList  +=  [ord(i) for i in u_chr]

if len(l_charList) == 0:
    print("It seems you don't want a password!")
else:
    l_password = ''
    #Generating the password based on the user list of characters.
    for i in range(u_length):
        l_password += chr(l_charList[randint(0, len(l_charList)-1)])

    print("Here is your password:\n\n", l_password, '\n', sep='')
