try:
    g_number=int(input("What is your number? "))

    if g_number <= 0:
        print("You must chose an integer bigger than 0!")

    def f_primes(l_number):

        for i in range(2,l_number): #l_number will not be tested, there is no need

            if l_number%i==0:
                return False
        
        return True 
        #if the for loop wont make a return,
        #then it means that l_number is not divisible by any other number between 1 and l_number

    for i in range(2, g_number+1):
        if f_primes(i):
            print(i, end=" ")
except:
    print("Something didn't go as expected!")    
