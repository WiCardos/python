import os
import random

print('What is the path?')
print('E.g.: c:\\myFolder')
u_dir = (input(''))

#To use the root directory
if len(u_dir) == 0:
    u_dir = os.getcwd()


l_allfiles = []

for path, folders, files in os.walk(u_dir):
    for f in files:
        if str(path)[-1] == '\\':
            l_allfiles.append(str(path) + str(f))
        else:
            l_allfiles.append(str(path) + '\\' + str(f))

l_val1 = True
while l_val1:
    l_val2 = True
    while l_val2:
        try:
            u_numFiles = int(input('What is the number of files you wish to retrieve?\n'))
            l_val2 = False
        except ValueError:
            print("You must choose an integer!")

    if u_numFiles > len(l_allfiles):
        print('You are trying to retrieve more files than currently exist in the folder structure!')
        print("There are only ", len(l_allfiles), " files in this folder structure!")
    else:
        l_val1 = False


l_returnFiles = []
l_count = 0

while l_count < u_numFiles:
    l_var = random.randint(0,len(l_allfiles)-1)
    l_returnFiles.append(l_allfiles[l_var])
    del l_allfiles[l_var]
    l_count += 1 

print('Here are your files:\n')

for i in l_returnFiles:
    print(i)
print('')
