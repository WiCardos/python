def read_int(prompt, min, max):
    l_validator = True
    while l_validator:
        try:
            l_value = int(input(prompt))
            if l_value not in range(min,max+1):
                print('Error: the value is not within permitted range ('+str(min)+'..'+str(max)+')')
            else:
                l_validator = False
        except ValueError:
            print('Error: wrong input')
    return l_value

l_variable = read_int("Enter a number from -10 to 10: ", -10, 10)

print("The number is:", l_variable)


#Test Data

#input1:
#Enter a number from -10 to 10: 100
#expected output:
#Error: the value is not within permitted range (-10..10)

#input2:
#Enter a number from -10 to 10: asd
#expected output:
#Error: wrong input

#input3:
#Enter number from -10 to 10: 1
#expected output:
#The number is: 1
