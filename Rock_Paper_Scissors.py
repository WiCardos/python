from random import randint

d_rockPaperScissor = {0:'Rock', 1:'Paper',2:'Scissor'}

l_tie = True
while l_tie:
    u_input = input("What is your choice? ")
    l_cpu = d_rockPaperScissor[randint(0,2)]

    print(l_cpu)

    if u_input.upper() == l_cpu.upper():
        print('Tie!')

    elif u_input.upper() == d_rockPaperScissor[0].upper():
        if l_cpu == d_rockPaperScissor[1]:
            print('You Lose!')
            l_tie = False
        else: #There is no need to have the validation, because there is only one cenario left
            print('You Win!')
            l_tie = False

    elif u_input.upper() == d_rockPaperScissor[1].upper():
        if l_cpu == d_rockPaperScissor[2]:
            print('You Lose!')
            l_tie = False
        else: #There is no need to have the validation, because there is only one cenario left
            print('You Win!')
            l_tie = False

    elif u_input.upper() == d_rockPaperScissor[2].upper():
        if l_cpu == d_rockPaperScissor[0]:
            print('You Lose!')
            l_tie = False
        else: #There is no need to have the validation, because there is only one cenario left
            print('You Win!')
            l_tie = False
    else:
        print("Your choice is not valid, try again:")
