from os import strerror

#creating the dicitionay with the letter counts
l_dictionaryCounts = {chr(i): 0 for i in range(ord('a'), ord('z') + 1)}

l_srcname = input("Enter the source file name: ")

try:
    for i in open(l_srcname, 'rt').read():
        l_dictionaryCounts[i.lower()]+=1
except IOError as e:
    print("Cannot open the source file: ", strerror(e.errno))
    exit(e.errno)

l_sortedDictionaryCounts=dict(sorted(l_dictionaryCounts.items(), key =lambda item: item[1], reverse= True))

try:
    l_outfile = open(l_srcname+'.hist', 'wt')
    for i in l_sortedDictionaryCounts:
        if l_sortedDictionaryCounts[i]>0:
            l_outfile.write(str(i) + '->' + str(l_dictionaryCounts[i]) +'\n')
    l_outfile.close()
except IOError as e:
    print("I/O error occurred: ", strerror(e.errno))

#Test Data
#file named 'samplefile.txt' (must be in the same dir as this script)
#with 'cBabAa'
#expected outputs:
#a -> 3
#b -> 2
#c -> 1
#file named 'samplefile.txt.hist' (in the same dir as this script)
