rowList = ['','','','','','','','','']
columnList = ['','','','','','','','','']
subsquares = ['','','','','','','','','']

checkString = '123456789'
l_ok = True

#Receiving the user inputs.
rowList[0] = input("What is your first row? ")
rowList[1] = input("What is your second row? ")
rowList[2] = input("What is your third row? ")
rowList[3] = input("What is your fourth row? ")
rowList[4] = input("What is your fifth row? ")
rowList[5] = input("What is your sixth row? ")
rowList[6] = input("What is your seventh row? ")
rowList[7] = input("What is your eighth row? ")
rowList[8] = input("What is your ninth row? ")

#Extracting the values to store as columns
for i in range(len(columnList)):
    for j in range(len(rowList)):
        columnList[i] += rowList[j][i]

#Extracting the values to store as subsquares
for k in range(len(subsquares)):
    for l in range(3):
        subsquares[k] += rowList[l+(3*(k//3))][((k%3)*3)] + rowList[l+(3*(k//3))][1+((k%3)*3)] + rowList[l+(3*(k//3))][2+((k%3)*3)]

#Checking if all the digits are in every row, column and subsquare.
for i in checkString:
    for r in range(len(rowList)):
        if i not in rowList[r]:
            l_ok = False
    for c in range(len(columnList)):
        if i not in columnList[c]:
            l_ok = False
    for s in range(len(subsquares)):
        if i not in subsquares[s]:
            l_ok = False

if not l_ok:
    print('No')
else:
    print('Yes')

#Test Data

#input1: 

#295743861
#431865927
#876192543
#387459216
#612387495
#549216738
#763524189
#928671354
#154938672

#expected output:   Yes
#input1:

#195743862
#431865927
#876192543
#387459216
#612387495
#549216738
#763524189
#928671354
#254938671

#expected output:   No
