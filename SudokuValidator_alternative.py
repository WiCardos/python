l_ok = True

def f_check(numbers):
    return ''.join(sorted(numbers)) == '123456789'


rowList = []

for r in range(9):
    ok = False
    while not ok:
        row = input("Enter row #" + str(r + 1) + ": ")
        ok = len(row) == 9 and row.isdigit()
        if not ok:
            print("Incorrect row data - 9 digits required")
    rowList.append(row)

for r in range(len(rowList)):
    if not f_check(rowList[r]):
        l_ok = False
        print('1')
        break

if l_ok:
    for c in range(9):
        column = [] #new column for every iteration
        for r in range(len(rowList)):
            column.append(rowList[r][c])
        if not f_check(column):
            l_ok = False
            break

if l_ok:
    for r in range(0, 9, 3):
        for c in range(0, 9, 3):
            sqr = '' #A new square for iteration
            for i in range(3):
                sqr += rowList[r+i][c:c+3]
            #Builds a square and validates it immediately
            if not f_check(sqr):
                l_ok = False
                break

if l_ok:
    print('Yes')
else:
    print('No')

#Test Data

#input1: 

#295743861
#431865927
#876192543
#387459216
#612387495
#549216738
#763524189
#928671354
#154938672

#expected output:   Yes
#input1:

#195743862
#431865927
#876192543
#387459216
#612387495
#549216738
#763524189
#928671354
#254938671

#expected output:   No
