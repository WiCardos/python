print("What is your birthday?")
userDate = input("Use a format where all digits are together, like YYYYMMDD or DDMMYYY: ")

l_Num = userDate #I could recyle the userDate variable, but I rather create a new varibale and keep the original data intact.

while len(l_Num) > 1:
    
    l_sumNum = 0    #initializes (the fisrt time) and resets this variable (every following time the loop is executed)
    
    for elem in l_Num:  #because l_Num is a string, and a string works like a list.
        l_sumNum += int(elem)
    
    l_Num = str(l_sumNum)   #resets the l_Num variable to be reevaluated by the while loop.

print("your digit of life is: ", l_sumNum)

#Test Data
#input: 19991229
#expected output:   6
#input: 20000101
#expected output:   4
