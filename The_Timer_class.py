class Timer:
    def __init__(self,hours=0,minutes=0,seconds=0):
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds

    def __str__(self):

        self.hours = str(self.hours)
        self.minutes = str(self.minutes)
        self.seconds = str(self.seconds)
        
        if len(self.hours)==1:
            self.hours = '0' + self.hours
        if len(self.minutes)==1:
            self.minutes = '0' + self.minutes
        if len(self.seconds)==1:
            self.seconds = '0' + self.seconds

        return self.hours + ':' \
               + self.minutes + ':' \
               + self.seconds

    def next_second(self):
        self.hours = int(self.hours)
        self.minutes = int(self.minutes)
        self.seconds = int(self.seconds)

        self.seconds += 1
        
        self.minutes += self.seconds//60
        self.seconds = self.seconds%60
        
        self.hours += self.minutes//60
        self.minutes = self.minutes%60
        
        self.hours = self.hours%24

    def previous_second(self):
        self.hours = int(self.hours)
        self.minutes = int(self.minutes)
        self.seconds = int(self.seconds)

        self.seconds -= 1

        self.minutes += self.seconds//60
        self.seconds = self.seconds%60
        
        self.hours += self.minutes//60
        self.minutes = self.minutes%60
        
        self.hours = self.hours%24


timer = Timer(23, 59, 59)
print(timer)
timer.next_second()
print(timer)
timer.previous_second()
print(timer)

#Test Data
#expected outputs:
#23:59:59
#00:00:00
#23:59:59
