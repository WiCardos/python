class Timer:
    def __init__(self,hours=0,minutes=0,seconds=0):
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds

    def __convert_str(self):
        self.hours = str(self.hours)
        self.minutes = str(self.minutes)
        self.seconds = str(self.seconds)

    def __convert_int(self):
        self.hours = int(self.hours)
        self.minutes = int(self.minutes)
        self.seconds = int(self.seconds)

    def __str__(self):
        #converts the variables to strings
        self.__convert_str()
        
        #if the variable is a single digit appends a 0
        if len(self.hours)==1:
            self.hours = '0' + self.hours
        if len(self.minutes)==1:
            self.minutes = '0' + self.minutes
        if len(self.seconds)==1:
            self.seconds = '0' + self.seconds

        #returns the string with the "hh:mm:ss" format
        return self.hours + ':' \
               + self.minutes + ':' \
               + self.seconds

    def __update_time(self):
        #adds the extra minute (if seconds > 59)
        self.minutes += self.seconds//60
        #resets the seconds counter
        self.seconds = self.seconds%60

        #adds the extra hour (if minutes > 59)
        self.hours += self.minutes//60
        #resets the minutes counter
        self.minutes = self.minutes%60

        #resets the hours counter
        self.hours = self.hours%24

    def next_second(self):
        #converts the variables to integers
        self.__convert_int()
        #adds the second
        self.seconds += 1
        #updates all variables
        self.__update_time()

    def previous_second(self):
        #converts the variables to integers
        self.__convert_int()
        #subtracts the second
        self.seconds -= 1
        #updates all variables
        self.__update_time()


timer = Timer(23, 59, 59)
print(timer)
timer.next_second()
print(timer)
timer.previous_second()
print(timer)

#Test Data
#expected outputs:
#23:59:59
#00:00:00
#23:59:59
