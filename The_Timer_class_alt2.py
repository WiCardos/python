class Timer:
    def __init__(self,hours=0,minutes=0,seconds=0):
        self.__hours = hours
        self.__minutes = minutes
        self.__seconds = seconds

    def __two_digits(self,i_val):
        v_string = str(i_val)
        if len(v_string) == 1:
            v_string = '0' + v_string
        return v_string

    def __str__(self):
        #returns the string with the "hh:mm:ss" format
        return self.__two_digits(self.__hours) + ':' \
               + self.__two_digits(self.__minutes) + ':' \
               + self.__two_digits(self.__seconds)

    def __update_time(self):
        #adds the extra minute (if seconds > 59)
        self.__minutes += self.__seconds//60
        #resets the seconds counter
        self.__seconds = self.__seconds%60

        #adds the extra hour (if minutes > 59)
        self.__hours += self.__minutes//60
        #resets the minutes counter
        self.__minutes = self.__minutes%60

        #resets the hours counter
        self.__hours = self.__hours%24

    def next_second(self):
        #adds the second
        self.__seconds += 1
        #updates all variables
        self.__update_time()

    def previous_second(self):
        #subtracts the second
        self.__seconds -= 1
        #updates all variables
        self.__update_time()


timer = Timer(23, 59, 59)
print(timer)
timer.next_second()
print(timer)
timer.previous_second()
print(timer)

#Test Data
#expected outputs:
#23:59:59
#00:00:00
#23:59:59
