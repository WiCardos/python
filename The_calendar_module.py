import calendar

class MyCalendar(calendar.Calendar):
    def count_weekday_in_year(self, l_year, l_weekday): #l_weekday = 0 is monday

        l_count = 0

        for month in range(12):
            for week in self.monthdays2calendar(l_year, month+1):
                for day in range(len(week)):
                    #if the day belongs to that month
                    if week[day][0] != 0 and\
                    l_weekday == week[day][1]: #if the current day is the same as the inputed day
                        l_count += 1

        return l_count

myobject = MyCalendar()
print(myobject.count_weekday_in_year(2019, 0))
print(myobject.count_weekday_in_year(2000, 6))

#Test Data
#year=2019, weekday=0
#year=2000, weekday=6
#expected outputs:
#52
#53
