from datetime import datetime

l_date = datetime(2020, 11, 4, 14, 53)

print(l_date.strftime("%Y/%m/%d %H:%M:%S"))
print(l_date.strftime("%y/%B/%d %H:%M:%S %p"))
print(l_date.strftime("%a, %Y %b %d"))
print(l_date.strftime("%A, %Y %B %d"))
print(l_date.strftime("Weekday: %w"))
print(l_date.strftime("Day of the year: %j"))
print(l_date.strftime("Week number of the year: %U"))

#expected outputs:

#2020/11/04 14:53:00
#20/November/04 14:53:00 PM
#Wed, 2020 Nov 04
#Wednesday, 2020 November 04
#Weekday: 3
#Day of the year: 309
#Week number of the year: 44
