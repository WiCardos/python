#importing the function that generates random numbers
from random import randrange

def display_board(board):
    # The function accepts one parameter containing the board's current status
    # and prints it out to the console.
    for row in range(len(board)):
        print("+-------+-------+-------+")
        print("|       |       |       |")
        print("|   ",end="")
        for column in range(len(board)):
           print(board[row][column],"   ",sep="   |",end="")
        print("\n|       |       |       |")
        print("+-------+-------+-------+")

def enter_move(board):
    # The function accepts the board's current status, asks the user about their move, 
    # checks the input, and updates the board according to the user's decision.
    l_number=int(input("Enter your move: "))
    l_row = d_coordinates[l_number][0]
    l_column = d_coordinates[l_number][1]
    
    while board[l_row][l_column] in ("X","O"):
        print("That move has already been played!")
        l_number=int(input("Enter your move: "))
        l_row = d_coordinates[l_number][0]
        l_column = d_coordinates[l_number][1]
    
    board[l_row][l_column] = 'O'
    print("Move accepted!")

#Not sure why we need this list    
def make_list_of_free_fields(board):
    # The function browses the board and builds a list of all the free squares; 
    # the list consists of tuples, while each tuple is a pair of row and column numbers.
    
    # first we clear the current list
    del lst_free_squares[:]
    # then we fill it    
    for row in range(len(board)):
        for column in range(len(board)):
            if board[row][column] not in ("X","O"):
                lst_free_squares.append((row,column))
    print(lst_free_squares)

def victory_for(board, sign):
    # The function analyzes the board's status in order to check if 
    # the player using 'O's or 'X's has won the game
    
    #for row victory
    if sign == "X" and \
    (  board[0].count(sign)==3 \
    or board[1].count(sign)==3 \
    or board[2].count(sign)==3):
        print("You lose!")
        return True
    elif sign == "O" and \
    (  board[0].count(sign)==3 \
    or board[1].count(sign)==3 \
    or board[2].count(sign)==3):
        print("You won!")
        return True

    
    #for column victory
    for column in range(len(board)):
        l_count=0
        for row in range(len(board)):
            if board[row][column] == sign:
                l_count += 1
            if l_count == 3 and sign == "X":
                print("You lose!")
                return True
            elif l_count == 3 and sign == "O":
                print("You won!")
                return True

    #for diagonal victory
    #since the program will always be X on 5 (or 1,1)
    #this only validates X victory
    if (board[0][0] == "X" and board[2][2] == "X") \
    or (board[0][2] == "X" and board[2][0] =="X"):
        print("You lose!")
        return True
    
    
    #for draw
    for row in range(len(board)):
        for column in range(len(board)):
            if board[row][column] in ("X","O"):
                l_count += 1
            if l_count == 9:
                print("It is a draw!")
                return True
            else:
                return False

def draw_move(board):
    # The function draws the computer's move and updates the board.

    #find some random coordinates
    l_row = randrange(3)
    l_column = randrange(3)
    
    #if those coordinates are filled, we search again
    while board[l_row][l_column] in ("X","O"):
        l_row = randrange(3)
        l_column = randrange(3)
    
    #once we find a pair of coordinate that are free we assign the move
    board[l_row][l_column] = 'X'
#dictionary with square's number coordinates
d_coordinates = {
    1:(0,0)
    ,2:(0,1)
    ,3:(0,2)
    ,4:(1,0)
    ,5:(1,1)
    ,6:(1,2)
    ,7:(2,0)
    ,8:(2,1)
    ,9:(2,2)
}

#starting board
board = [
    [1,2,3]
    ,[4,'X',6]
    ,[7,8,9]
]

#list of free squares
lst_free_squares = []

display_board(board)
while not victory_for(board, "X"):
    #make_list_of_free_fields(board)
    enter_move(board)
    display_board(board)
    if victory_for(board, "O"):
        print("Game Over!")
        exit()
    draw_move(board)
    display_board(board)
    if victory_for(board, "X"):
        print("Game Over!")
        exit()

