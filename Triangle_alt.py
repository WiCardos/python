import math

class Point:
    def __init__(self, x=0.0, y=0.0):
        self.__x = float(x)
        self.__y = float(y)

    def getx(self):
        return self.__x
    
    def gety(self):
        return self.__y

    def distance_from_xy(self, x, y):
        return math.hypot(abs(x-self.__x), abs(y-self.__y))

    def distance_from_point(self, point):
        return math.hypot(abs(Point.getx(point)-self.__x),
                          abs(Point.gety(point)-self.__y))

class Triangle:
    def __init__(self, point_a, point_b, point_c):
        self.__list = [point_a, point_b, point_c]
        
    def perimeter(self):
        l_var = 0
        for i in range(3):
            l_var += self.__list[i].distance_from_point(self.__list[(i+1)%3])
        return l_var

triangle = Triangle(Point(0, 0), Point(1, 0), Point(0, 1))
print(triangle.perimeter())

#Test Data
#expected outputs:
#3.414213562373095
