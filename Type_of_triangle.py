def f_type_of_triangle(l_side_a, l_side_b, l_side_c):
    if u_side_a == u_side_b == u_side_c:
        l_output = "Equilateral"
    elif u_side_a == u_side_b or u_side_a == u_side_c or u_side_b == u_side_c:
        l_output = "Isosceles"
    else:
        l_output = "Scalene"
    return l_output

l_var = True
while l_var:
    try:
        u_side_a = float(input("What is the length of the first side of the triangle? "))
        u_side_b = float(input("What is the length of the second side of the triangle? "))
        u_side_c = float(input("What is the length of the last side of the triangle? "))

        print("Your triangle is ", f_type_of_triangle(u_side_a, u_side_b, u_side_c), "!", sep='')
        l_var = False
    except ValueError:
        print("You must insert a float!")
