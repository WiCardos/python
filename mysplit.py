def mysplit(strng):
    
    splited = [] #a list to store the split   
    
    found = strng.find(' ')  #a variable that will store the location (index) of the white space
    
    i = 0 #a variable that stores the start (the initial index) of the substring we want to extract

    while found != -1: #while we have white spaces on the received string
    
        if strng[i:found] not in ('', ' '): #to prevent appending white spaces
            splited.append(strng[i:found])
    
        i = found+1 #increments the current starting index to the stop index 
    
        found = strng.find(' ', found + 1) #increments the found to the next found whitespace
    
    if strng[i:found] not in ('', ' '): #to prevent appending white spaces
        splited.append(strng[i:])
    
    return splited

#test data
print(mysplit("To be or not to be, that is the question"))
print(mysplit("To be or not to be,that is the question"))
print(mysplit("   "))
print(mysplit(" abc "))
print(mysplit(""))
